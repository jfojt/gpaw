.. _energetics:

==========
Energetics
==========

.. toctree::
   :maxdepth: 2

   ../H2/atomization
   hubbardu/hubbardu
   defects/defects
   rpa_tut/rpa_tut
   rpa_ex/rpa
   fxc_correlation/rapbe_tut
   rangerpa/rangerpa_tut
