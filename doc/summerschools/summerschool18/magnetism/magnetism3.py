# %%
"""
# Discover a new 2D material with high critical temperature

Now that you know how to calculate exchange coupling constants and anisotropies, you are in a position to search for new magnetic materials yourself.

Go to the 2D database at https://cmrdb.fysik.dtu.dk/?project=c2db and search for ferromagnetic magnetic (FM) or anti-ferromagnetic (AFM) materials with finite band gaps. (The theory developed so far is not applicable to metals, since these typically have long range interactions). Click the Add Column button on the right and add the magnetic anisotropy. You should now be able to sort the 2D materials according to anisotropy and choose a material with a large value. A large anisotropy is expected to lead to a high critical temperature. Large values of $J$ and $S$ are also desirable and $J$ may be estimated from the deviation from the paramagnetic state stated at the webpage.

Decide on a material you find promising and perform the analysis from the previous notebooks. I suggest you do not choose an anti-ferromagnetic hexagonal material, since the non-collinear ground state makes the analysis somewhat complicated. Calculate $J$ and $A$ and estimate the critical temperature from the expression stated in part 1. As a rough estimate you may also use the expression for anti-ferromagnetic systems if that is what you decide on.
"""

# %%
